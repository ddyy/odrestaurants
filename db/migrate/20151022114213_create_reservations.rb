class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.datetime :begin_at
      t.references :table, index: true
      t.integer :guest_count

      t.timestamps null: false
    end
    add_foreign_key :reservations, :tables
  end
end
