class Reservation < ActiveRecord::Base
  belongs_to :table

  validates :table_id, presence: true
  validates :begin_at, presence: true
  validates :guest_count, presence: true

  def initialize(attributes = nil, options = {})

    table = Restaurant.find(attributes[:restaurant_id]).find_open_table(attributes[:begin_at], attributes[:guest_count])
    if (table)
      attributes[:table_id] = table.id
    end
    attributes.tap { |hs| hs.delete(:restaurant_id) }
    super

  end
end
