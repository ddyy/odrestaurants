# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

restaurant = Restaurant.create(id: 1, name: 'Bestfood Restaurant')

Table.create(restaurant_id: 1, seat_count: 3)
Table.create(restaurant_id: 1, seat_count: 4)
Table.create(restaurant_id: 1, seat_count: 5)
Table.create(restaurant_id: 1, seat_count: 6)