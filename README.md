# Restaurant Reservations #

### Set Up ###

* clone repo
* set db configuration in /config/database.yml
* bundle install
* rake db:create
* rake db:migrate
* rake db:seed

### Make reservation API request ###

POST /reservations.json

Payload
```
#!json

{"reservation": { "guest_count": 4, "begin_at": "2015-12-11 10:10:10", "restaurant_id": 1 }}
```


* restaurant_id: id of the restaurant
* begin_at: reservation time in 'Y-m-d H:i:s' format
* guest_count: number of guests


Example response:


```
#!json

{
id: 29
begin_at: "2015-12-11T10:10:10.000Z"
table_id: 5
guest_count: 4
created_at: "2015-10-22T14:21:17.406Z"
updated_at: "2015-10-22T14:21:17.406Z"
}

```

* id: is the reservation ID for future lookup

* Returns HTTP 201 if table is found and reservation is created
* Returns HTTP 422 if no tables are available for the specified guest count and time

### Assumptions ###

* Restaurant is open 24 hours
* Reservations are made for 2 hour blocks of time
* By default, 1 restaurant is created with 4 tables
* Tables must have enough seats for the guest to make a reservation