class Table < ActiveRecord::Base
  belongs_to :restaurant
  has_many :reservations

  def is_available(begin_at)

    if self.reservations.where(begin_at: (begin_at.to_time(:utc) - 2.hours).to_datetime..(begin_at.to_time(:utc) + 1.minute).to_datetime).count > 0
      return false
    end
    return true
  end
end
