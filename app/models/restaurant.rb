class Restaurant < ActiveRecord::Base
  has_many :tables
  has_many :reservations, through: :tables

  def find_open_table(begin_at, guest_count)

    self.tables.where("seat_count >= ?", guest_count).each do |t|
      if t.is_available(begin_at)
        return t
      end
    end
    nil
  end
end
