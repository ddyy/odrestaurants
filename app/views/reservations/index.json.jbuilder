json.array!(@reservations) do |reservation|
  json.extract! reservation, :id, :begin_at, :table_id, :guest_count
  json.url reservation_url(reservation, format: :json)
end
